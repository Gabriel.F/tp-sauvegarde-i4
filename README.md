## TP système de sauvegarde - compte rendu

# Configuration & instructions préalables
* Rsync est nécessaire (déjà installé sur les deux VMs)
* Il faut modifier les variables de configuration présentes en début de script
* Il faut avoir généré une paire de clés SSH afin de faire communiquer les deux serveurs de manière sécurisée sans avoir à valider de mot de passe (nécessaire pour les CRON)
    * Génération de la paire de clés : ```ssh-keygen -t rsa -b 2048```
    * Copie de la clé publique vers le serveur : ```ssh-copy-id user@host```
* Création d'un serveur esclave pour la base de données MySQL
    * Configuration nécessaire
        * Fichier de configuration MySQL
            * Mettre en bind l'adresse réelle du serveur au lieu de localhost
            * Changement de l'ID du serveur maître et activation des logs binaires
            * Relancer le démon MySQL, donner les privilèges à la base esclave
            * Bloquer en écriture la base maître, faire un dump, débloquer en écriture
            * Configurer la base esclave : changer l'adresse bindée, changer l'ID du serveur, activer les logs binaires, redémarrer le démon MySQL
            * Activer la réplication puis démarrer le serveur esclave
    * Le serveur esclave joue donc le rôle d'une sauvegarde continue pour la base MySQL, étant donné que chaque modification est répliquée. Nous ne nous soucierons donc que de sauvegardes complètes et incrémentielles dans notre script


# Procédure d'utilisation (simplifiée)
* Sauvegardes horodatées et classées par semaine, avec un répertoire pour la base de données et un autre pour l'application Nextcloud
    * Archivage et compression avec GZIP pour maximiser le gain de place
* Stratégie adoptée pour la sauvegarde incrémentielle de la base de données
    * Utilisation des logs binaires (qui contiennent toutes les transactions SQL effectuées)
* Fonctionnement du script shell
    * Exécution comme un script de manière classique, il faut cependant passer en paramètre soit "full" pour effectuer des sauvegardes complètes ou bien "partial" pour effectuer des incrémentales (s'ils sont mal ou non renseignés, un message d'erreur apparaît)
* Automatisation de la sauvegarde : mise en place des CRON
    * Sauvegarde complète tous les lundis à 8h : ```0 8 * * 1 . /home/epsi/save.sh full```
    * Sauvegarde incrémentielle tous les jours à 9h : ``` 0 9 * * * . /home/epsi/save.sh partial```
* Les sauvegardes ne sont gardées qu'un maximum de 35 jours (paramétrable)

    
# Procédure de restauration (simplifiée)
* Application Nextcloud
    1. Choisir la semaine pour laquelle on souhaite restaurer des sauvegardes
    2. Restaurer la sauvegarde complète en faisant une simple extraction de l'archive vers le dossier Nextcloud (utilitaire tar)
        * Rcp ou Rsync peuvent être utilisés pour effectuer une copie à distance
    3. Suivre le même principe pour toutes les sauvegardes incrémentales (les appliquer dans l'ordre chronologique) en faisant une extraction vers le dossier Nextcloud (l'utilitaire tar écrase par défaut les fichiers présents par ceux copiés)
        * Rcp ou Rsync peuvent également être utilisés pour effectuer une copie à distance
    4. C'est tout !
* Base de données MySQL
    1. Choisir la semaine pour laquelle on souhaite restaurer des sauvegardes
    2. Restaurer la sauvegarde complète avec l'utilitaire `mysqldump`
        * Exemple : ```mysql -u {user} -p < ma_sauvegarde_complete.dump```
    3. Restaurer toutes les sauvegardes incrémentales les unes à la suite des autres par ordre chronologique grâce à l'utilitaire `mysqlbinlog`
        * Exemple : ```mysqlbinlog /var/lib/mysql-bin.000001 | mysql –uroot –proot```