#!/bin/bash

# CONSTANTS
LABEL_PARTIAL="partial"
LABEL_FULL="full"

# CONFIGURATION
BACKUPS_TTL=35 # In days

NEXTCLOUD_SERV_ADDRESS="192.168.247.128"
NEXTCLOUD_SERV_USER="root"
NEXTCLOUD_APP_DIR="/var/www/html/nextcloud/"
NEXTCLOUD_BACKUPS_DIR="/home/epsi/backups"

MYSQL_DB="nextcloud"
MYSQL_USER="root"
MYSQL_PASS="root"
MYSQL_BINARY_LOGS_NAME="mysql-bin"

# SCRIPT
timestamp=`date +"%d_%m_%Y_%Hh%Mm%Ss"`
current_week=`date +week_%V_%Y`

if [[ $1 != $LABEL_FULL && $1 != $LABEL_PARTIAL ]]
then
    echo "Wrong argument : either choose '$LABEL_FULL' or '$LABEL_PARTIAL'"
    return
fi
current_week_dir=$NEXTCLOUD_BACKUPS_DIR/$current_week
mkdir -p $current_week_dir

backups_trash_dir="/tmp/backups_trash"
mkdir -p $backups_trash_dir

mkdir -p $current_week_dir/nextcloud
mkdir -p $current_week_dir/database

case "$1" in
    $LABEL_PARTIAL)
        mkdir -p $backups_trash_dir/incremental
        mkdir -p $backups_trash_dir/complete
        dir_files_count=`ls $current_week_dir/nextcloud -1 | wc -l`
        # Comparing either to a complete or incremental backup
        find $current_week_dir/nextcloud -type f -name "complete*" -exec tar -xzf {} -C $backups_trash_dir/complete \;
        if [ $dir_files_count -gt 1 ]; then
            find $current_week_dir/nextcloud -type f -name "incremental*" -exec tar -xzf {} -C $backups_trash_dir/complete \;
        fi
        echo "Copying files..."
        rsync -a --compare-dest=$backups_trash_dir/complete --delete $NEXTCLOUD_SERV_USER@$NEXTCLOUD_SERV_ADDRESS:$NEXTCLOUD_APP_DIR $backups_trash_dir/incremental
        find $backups_trash_dir/incremental -type d -empty -delete
        echo "Creating an archive..."
        tar -czf $current_week_dir/nextcloud/incremental$dir_files_count-$timestamp.tar.gz -C $backups_trash_dir/incremental .
        # DB incremental backup
        echo "Backing up database..."
        echo "Cleaning..."
        rm -Rf $backups_trash_dir/
        ;;
    $LABEL_FULL) 
        echo "Copying files..."
        rsync -a --delete $NEXTCLOUD_SERV_USER@$NEXTCLOUD_SERV_ADDRESS:$NEXTCLOUD_APP_DIR/ $backups_trash_dir/
        echo "Creating an archive..."
        tar -czf $current_week_dir/nextcloud/complete-$timestamp.tar.gz -C $backups_trash_dir .
        # DB complete backup
        echo "Dumping the database..."
        dump_filename="complete-$timestamp.dump"
        ssh $NEXTCLOUD_SERV_USER@$NEXTCLOUD_SERV_ADDRESS "mysqldump -u$MYSQL_USER -p$MYSQL_PASS $MYSQL_DB > /tmp/$dump_filename"
        rsync $NEXTCLOUD_SERV_USER@$NEXTCLOUD_SERV_ADDRESS:/tmp/$dump_filename $current_week_dir/database/
        echo "Cleaning..."
        rm -Rf $backups_trash_dir/
        ;;
esac

echo "Deleting old backups..."
find $NEXTCLOUD_BACKUPS_DIR/* -type d -mtime +$BACKUPS_TTL -exec -rm -rf {} \;
echo "Done !"